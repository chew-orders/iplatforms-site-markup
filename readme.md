Ставим пакетный менеджер Yarn: https://yarnpkg.com/en/docs/install

Устанавливаем необходимые пакеты

```sh
yarn
```

Для сборки стилей запускаем sass cli (он создаст папку app/css; если нужен вотчер используем аргумент -w):
```bash
node-sass src/sass/main.sass src/static/css/styles.css
```

## Docker

```js
docker-compose up --build --remove-orphans
```

Команда запустит hugo в режиме watcher'а. Собранный сайт будет доступен по [127.0.0.1:1313](http://127.0.0.1:1313).

Временно, sass-стили придется собирать вручную. 